// extern crate time;

use std::str;

/*
use std::fs::OpenOptions;
use std::fs::File;
use std::io::Read;
use std::io::Write;
use std::iter::FromIterator;
use std::collections::HashSet;
use std::cmp::max;
*/

use matrix_bot_api::handlers::{HandleResult, Message, StatelessHandler};
use matrix_bot_api::{MatrixBot, ActiveBot, MessageType};

const TERRA_URL: &str = "http://terra.finzdani.net/api/sites/";

/*
const CACHE_FILE: &str = ".cache/terrabot-cache";

fn get_site_json() -> Vec<serde_json::Value> {
    let resp = reqwest::blocking::get(TERRA_URL);

    return resp.unwrap().json().unwrap();
}

fn get_cache_json() -> Vec<serde_json::Value> {
    let mut cache_file = home::home_dir().unwrap();

    cache_file.push(CACHE_FILE);

    let cf_handle = OpenOptions::new()
        .read(true)
        .write(true)
        .open(cache_file.clone());

    let mut ok_handle = match cf_handle {
        Err(_why) => return Vec::new(),
        Ok(val)   => val
    };

    let mut contents = String::new();

    //if this fails, something's wrong, so we can panic.
    ok_handle.read_to_string(&mut contents).ok();

    return serde_json::from_str(&contents).unwrap(); //if everything works, just return the vec.
}

fn save_to_cache(str_to_save: String) -> () {

    let mut cache_file = home::home_dir().unwrap();
        // unwrapping above is ok, since we want to panic
        // if there's no homedir.
    cache_file.push(CACHE_FILE);

    let mut new_cf_handle = OpenOptions::new()
        .write(true)
        .create(true)
        .open(cache_file.clone())
        .unwrap();

    new_cf_handle.write_all(&str_to_save.into_bytes()).ok();
    return ();
}

fn get_sorted_diff(vec1: Vec<serde_json::Value>, vec2: Vec<serde_json::Value>) -> Vec<(u64, String)> {

    let mut set1: HashSet<(u64, String)> = HashSet::new();
    let mut set2: HashSet<(u64, String)> = HashSet::new();

    for x in vec1.iter() {
        set1.insert((x["id"].as_u64().unwrap(), x["url"].to_string()));
    }

    for x in vec2.iter() {
        set2.insert((x["id"].as_u64().unwrap(), x["url"].to_string()));
    }

    let diff = set1.difference(&set2);

    let mut sorted_diff: Vec<(u64, String)> = Vec::new();

    for x in diff {
        sorted_diff.push(x.clone());
    }

    sorted_diff.sort();

    return sorted_diff;
}

fn check_for_new() {
    //Steps:
    //    1. perform the request
    let site_json = get_site_json();
    let str_json = serde_json::to_string(&site_json).unwrap();

    //    2. load the file and convert its contents to json
    let cache_json = get_cache_json();
    if cache_json.len() == 0 {
        //this means that there was nothing in the cache
        // or it didn't exist. So we'll save the response
        // to it and return early.
        save_to_cache(str_json);
        return ();
    }

    //    3. find the difference (ideally as a slice)

    let sorted_diff = get_sorted_diff(site_json, cache_json);
    
    //    4. convert the slice to an html message
    let mut response = String::new();
    response += "<pre><code>Recently added to Terra:";

    for (_, url) in sorted_diff.iter().rev() {
        response += "\n";
        response += url;
    }
    response += "</code></pre>";


//    bot.send_html_message(&response, &response, &message.room, MessageType::TextMessage);
}

The above functions aren't used at all currently, since I didn't consider the architecture
of the matrix_bot_api library when I wrote it all...

*/
fn terra_list_five(bot: &ActiveBot, message: &Message, _tail: &str) -> HandleResult {

    let resp = reqwest::blocking::get(TERRA_URL);

    let result = match resp {
            Err(_why) => return HandleResult::StopHandling,
            Ok(val)   => match val.json::<Vec<serde_json::Value>>() {
                Err(_why) => return HandleResult::StopHandling,
                Ok(json)  => json
            }
    };

    let num_sites = result.len();
    let slice;

    if num_sites <= 5 {
        slice = &result[..];
    }
    else {
        slice = &result[(num_sites - 5)..];
    }

    let mut response = String::new();
    response += "<pre><code>Recently added to Terra:";

    for x in slice.iter().rev() {
        let url = match x["url"].as_str() {
            None      => "missing url",
            Some(val) => val
        };
        response += "\n";
        response += url;
    }
    response += "</code></pre>";

    bot.send_html_message(&response, &response, &message.room, MessageType::TextMessage);
    HandleResult::StopHandling
}

fn main() {
    println!("Starting terrabot");

    let mut settings = config::Config::default();
    settings.merge(config::File::with_name("config.toml")).unwrap();

    let user = settings.get_str("user").unwrap();
    let password = settings.get_str("password").unwrap();
    let homeserver_url = settings.get_str("homeserver_url").unwrap();

    let mut handler = StatelessHandler::new();

    handler.register_handle("terra", terra_list_five);

    let bot = MatrixBot::new(handler);

//  more misguided design that doesn't work lol.
    //this is setting the bots interval function and duration.
//    bot.interval_f = Some(check_for_new);
//    bot.interval = Some(time::Duration::minutes(5));
//    bot.interval = Some(time::Duration::seconds(5));

    bot.run(&user,&password,&homeserver_url);
}
